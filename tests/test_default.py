import testinfra.utils.ansible_runner
import json

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_awx_containers(Command, Sudo):
    with Sudo():
        cmd = Command("set +x; docker ps --format \{\{.Names\}\}")
    assert cmd.rc == 0
    # Get the names of the running containers
    names = cmd.stdout.splitlines()
    print names
    assert all(name in names for name in [u'awx_memcached', u'awx_postgres', u'awx_rabbitmq', u'awx_task', u'awx_web', u'traefik_proxy'])


def test_awx_api(Command):
    # This tests that traefik forwards traffic to the AWX web server
    # and that we can access the AWX REST api
    cmd = Command('curl -k https://ics-ans-role-awx-default/api/v2/ping/')
    ping = json.loads(cmd.stdout)
    assert len(ping['instance_groups']) > 0 and len(ping['instance_groups'][0]['instances']) > 0
